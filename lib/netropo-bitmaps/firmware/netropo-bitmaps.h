#ifndef NETROPO_BITMAPS_H
#define NETROPO_BITMAPS_H

struct RGBColor {

  uint8_t r;
  uint8_t g;
  uint8_t b;
};

struct RGBPixel {

  uint16_t x;
  uint16_t y;
  struct RGBColor *c;
};

#endif // NETROPO_BITMAPS_H
