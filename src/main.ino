#include "neopixel-matrix.h"

#define MATRIX_1_PIN D2
#define MATRIX_1_SIZE 8
#define MATRIX_1_TYPE WS2812B

#define MATRIX_2_PIN D3
#define MATRIX_2_SIZE 8
#define MATRIX_2_TYPE WS2812B

#define BRIGHTNESS 32

Netropo_NeoPixel_Matrix matrix_1 = Netropo_NeoPixel_Matrix(MATRIX_1_SIZE, MATRIX_1_PIN, MATRIX_1_TYPE, 180);
Netropo_NeoPixel_Matrix matrix_2 = Netropo_NeoPixel_Matrix(MATRIX_2_SIZE, MATRIX_2_PIN , MATRIX_2_TYPE, 180);

RGBColor fill = {255, 255, 0};
RGBColor background = {0, 0, 0};
RGBColor altFill = {0, 0, 255};

void setup() {

  pinMode(MATRIX_1_PIN, OUTPUT);
  pinMode(MATRIX_2_PIN, OUTPUT);

  matrix_1.begin();
  matrix_1.show();

  matrix_2.begin();
  matrix_2.show();

  Serial.begin(9600);
}


void loop(){

  uint8_t i, j;

  matrix_1.clear();
  matrix_1.show();

  matrix_2.clear();
  matrix_2.show();

  for (i=0; i<MATRIX_1_SIZE; i++) {
    for (j=0; j<MATRIX_1_SIZE; j++) {
      matrix_1.setPixelColor(i, j, 0, 255, 0);
      matrix_2.setPixelColor(i, j, 0, 255, 0);

      matrix_1.show();
      matrix_2.show();
      delay(200);
    }
  }

}
